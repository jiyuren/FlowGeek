package org.thanatos.flowgeek.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ImageSpan;
import android.view.KeyEvent;
import android.widget.EditText;

import org.thanatos.flowgeek.bean.EmotionRules;

/**
 * Created by thanatos on 16/2/3.
 */
public class InputHelper {

    public static void backspace(EditText input) {
        if (input == null) {
            return;
        }
        KeyEvent event = new KeyEvent(0, 0, 0, KeyEvent.KEYCODE_DEL, 0, 0, 0, 0, KeyEvent.KEYCODE_ENDCALL);
        input.dispatchKeyEvent(event);
    }

    /**
     * 表情输入至文字中
     * @param resources
     * @param emotion
     * @return
     */
    @SuppressWarnings("all")
    public static Spannable display(Resources resources, EmotionRules emotion) {
        String remote = emotion.getRemote();
        Spannable spannable = new SpannableString(remote);
        Drawable d = resources.getDrawable(emotion.getMResId());
        d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
        ImageSpan iSpan = new ImageSpan(d, ImageSpan.ALIGN_BASELINE);
        //Spanned.SPAN_EXCLUSIVE_EXCLUSIVE 前后输入的字符都不应用这种Spannable
        spannable.setSpan(iSpan, 0, remote.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannable;
    }

    public static Spannable input2posts4image(Context context, String text, Bitmap bitmap){
        Spannable spannable = new SpannableString(text);
        ImageSpan imageSpan = new ImageSpan(context, bitmap);
        spannable.setSpan(imageSpan, 0, text.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannable;
    }
}
