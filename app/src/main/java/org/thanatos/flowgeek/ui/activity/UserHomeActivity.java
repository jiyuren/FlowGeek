package org.thanatos.flowgeek.ui.activity;

import org.thanatos.base.ui.activity.BaseHoldBackActivity;

/**
 * Created by thanatos on 16/2/19.
 */
public class UserHomeActivity extends BaseHoldBackActivity{

    public static final String BUNDLE_USER_KEY = "BUNDLE_USER_KEY";

    @Override
    protected String onSetTitle() {
        return null;
    }

    @Override
    protected int onBindLayout() {
        return 0;
    }

}
